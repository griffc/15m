# Example Game API

### Write an abstract class or interface that models the game API

#### Interface
src/Api/ApiInterface.php

#### Abstract class, implements above
src/Api/Api.php

### Write a factory to create two different types of game API

src/Api/ApiFactory
- supplied with simple constant thats used in a switch statement to determine which implemenation to create and return
- guzzle client is also passed (an exmaple of DI)

### Implement a class that models the provided API

#### Model
src/Api/Models
- simple POPOs containing the attributes etc.

### Write a test that tests your classes

#### Factory test
tests/Api/ApiFactoryTest.php
- checks correct object type gets created
- checks for exception case

#### 'Enter' Function Test
src/Api/EnterTest.php
- mocks the guzzle client, sets expected behaviours

### Create a docker container for your code and instructions on how to run it

- example docker stack in docker-compose.yml
- start with: docker-compose up -d 
- exec bash in the fpm container: docker exec -it 15m-php-fpm
- install dependencies: composer install
- run the tests: vendor/bin/phpunit

### Upload your code in GitHub or Bitbucket and provide us with the link
- url: https://bitbucket.org/griffc/15m
- my code is on a feature branch (git flow example) feature/game-api

### Build a simple CI/CD pipeline
- created bitbucket pipeline
- runs tests upon push
- for CD created a simple manual triggered step (that does nothing) - if tests fail you can't deploy 


