<?php

namespace GameApiExample\Api;

use GuzzleHttp\Client;
use SimpleXMLElement;
use DateTime;
use Exception;

abstract class Api implements ApiInterface
{

    /** @var Client  */
    private $client;

    /** @var string */
    private $sessionId;

    /** @var DateTime */
    private $requestTime;

    public function __construct(Client $client)
    {
       $this->client = $client;
    }

    /**
     * @param string $uri
     * @param string $body
     * @return string
     */
    public function doPost(string $uri, string $body)
    {

        try {

            $response = $this->client->request('POST', $uri, ['body' => $body]);

            if(null !== $response && null !== $response->getBody()) {
                return $response->getBody()->getContents() ?? null;
            }


        } catch (Exception $ex) {
            // something broke .. log etc..
        }

        return null;

    }


    public function createGSRequest(string $sessionId): SimpleXMLElement
    {
        $xml = new SimpleXMLElement('<server></server>');
        $xml->addAttribute('session', $sessionId);
        $xml->addAttribute('time', $this->getRequestTime()->format('c'));

        return $xml;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return Api
     */
    public function setClient(Client $client): Api
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

    /**
     * @param string $sessionId
     * @return Api
     */
    public function setSessionId(string $sessionId): Api
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getRequestTime(): DateTime
    {
        return $this->requestTime;
    }

    /**
     * @param DateTime $requestTime
     * @return Api
     */
    public function setRequestTime(DateTime $requestTime): Api
    {
        $this->requestTime = $requestTime;
        return $this;
    }




}