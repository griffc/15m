<?php

namespace GameApiExample\Api\Models;

class Ident
{

    private $id;

    private $guid;

    /**
     * Ident constructor.
     * @param $id
     * @param $guid
     */
    public function __construct($id, $guid)
    {
        $this->id = $id;
        $this->guid = $guid;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Ident
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     * @return Ident
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }



}
