<?php

namespace GameApiExample\Api\Models;

class Enter
{

    /** @var Ident */
    private $ident;

    private $key;

    /** @var Game */
    private $game;

    /**
     * Enter constructor.
     * @param Ident $ident
     * @param $key
     * @param Game $game
     */
    public function __construct(Ident $ident, $key, Game $game)
    {
        $this->ident = $ident;
        $this->key = $key;
        $this->game = $game;
    }

    /**
     * @return Ident
     */
    public function getIdent(): Ident
    {
        return $this->ident;
    }

    /**
     * @param Ident $ident
     * @return Enter
     */
    public function setIdent(Ident $ident): Enter
    {
        $this->ident = $ident;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return Enter
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return Game
     */
    public function getGame(): Game
    {
        return $this->game;
    }

    /**
     * @param Game $game
     * @return Enter
     */
    public function setGame(Game $game): Enter
    {
        $this->game = $game;
        return $this;
    }




}
