<?php

namespace GameApiExample\Api\Models;

class Game
{

    private $name;

    /**
     * Game constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }



}
