<?php

namespace GameApiExample\Api\Playson;

use GameApiExample\Api\Api;
use GameApiExample\Api\Models\Enter;
use GameApiExample\Api\Models\Ident;
use GameApiExample\Api\Models\Refund;
use GameApiExample\Api\Models\RoundBetWin;

class PlaysonApiTypeOne extends Api
{

    /**
     * @param Enter $enter
     * @return string
     */
    public function enter(Enter $enter): ?string
    {
        // create GS request
        $xml = $this->createGSRequest($this->getSessionId());

        // add enter and its attributes
        $enterXml = $xml->addChild('enter');
        $enterXml->addAttribute('id', $enter->getIdent()->getId());
        $enterXml->addAttribute('guid', $enter->getIdent()->getGuid());
        $enterXml->addAttribute('key', $enter->getKey());

        $gameXml = $enterXml->addChild('game');
        $gameXml->addAttribute('name', $enter->getGame()->getName());

        return $this->doPost('/enter', $xml->asXML());
    }


    /**
     * @param Ident $ident
     * @return string
     */
    public function getBalance(Ident $ident): ?string
    {
        // TODO: Implement getBalance() method.
    }

    /**
     * @param RoundBetWin $roundBetWin
     * @return string
     */
    public function roundBetWin(RoundBetWin $roundBetWin): ?string
    {
        // TODO: Implement roundBetWin() method.
    }

    /**
     * @param Ident $ident
     * @return string
     */
    public function logout(Ident $ident): ?string
    {
        // TODO: Implement logout() method.
    }

    /**
     * @param Refund $refund
     * @return string
     */
    public function refund(Refund $refund): ?string
    {
        // TODO: Implement refund() method.
    }
}