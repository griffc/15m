<?php

namespace GameApiExample\Api\Playson;

use GameApiExample\Api\Api;
use GameApiExample\Api\Models\Enter;
use GameApiExample\Api\Models\Ident;
use GameApiExample\Api\Models\Refund;
use GameApiExample\Api\Models\RoundBetWin;

/**
 * just stubs in here .. demonstrates alternative implementation
 * Class PlaysonApiTypeTwo
 * @package GameApiExample\Api\Playson
 */
class PlaysonApiTypeTwo extends Api
{


    /**
     * @param Enter $enter
     * @return string
     */
    public function enter(Enter $enter): ?string
    {
        // TODO: Implement enter() method.
    }

    /**
     * @param Ident $ident
     * @return string
     */
    public function getBalance(Ident $ident): ?string
    {
        // TODO: Implement getBalance() method.
    }

    /**
     * @param RoundBetWin $roundBetWin
     * @return string
     */
    public function roundBetWin(RoundBetWin $roundBetWin): ?string
    {
        // TODO: Implement roundBetWin() method.
    }

    /**
     * @param Ident $ident
     * @return string
     */
    public function logout(Ident $ident): ?string
    {
        // TODO: Implement logout() method.
    }

    /**
     * @param Refund $refund
     * @return string
     */
    public function refund(Refund $refund): ?string
    {
        // TODO: Implement refund() method.
    }
}