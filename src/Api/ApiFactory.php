<?php 

namespace GameApiExample\Api;

use GameApiExample\Api\Playson\PlaysonApiTypeOne;
use GameApiExample\Api\Playson\PlaysonApiTypeTwo;
use GuzzleHttp\Client;
use RuntimeException;

class ApiFactory
{

    const PLAYSON_TYPE_ONE = 1;
    const PLAYSON_TYPE_TWO = 2;

    public static function create(Client $client, string $apiType) : ApiInterface
    {

        switch($apiType) {

            case(static::PLAYSON_TYPE_ONE): {

                return new PlaysonApiTypeOne($client);

                break;
            }

            case(static::PLAYSON_TYPE_TWO): {

                return new PlaysonApiTypeTwo($client);

                break;
            }

            // unknown api type, so throw an exception
            default:
                throw new RuntimeException('invalid api type');
        }
        
    }
}