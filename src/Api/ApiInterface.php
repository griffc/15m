<?php

namespace GameApiExample\Api;

use GameApiExample\Api\Models\Enter;
use GameApiExample\Api\Models\Ident;
use GameApiExample\Api\Models\Refund;
use GameApiExample\Api\Models\RoundBetWin;
use GuzzleHttp\Client;
use DateTime;

/**
 * Interface ApiInterface
 * @package GameApiExample\Api
 */
interface ApiInterface 
{

    /**
     * ApiInterface constructor.
     * @param Client $client
     */
    public function __construct(Client $client);

    /**
     * @param Enter $enter
     * @return string
     */
    public function enter(Enter $enter) : ?string;

    /**
     * @param Ident $ident
     * @return string
     */
    public function getBalance(Ident $ident): ?string;

    /**
     * @param RoundBetWin $roundBetWin
     * @return string
     */
    public function roundBetWin(RoundBetWin $roundBetWin): ?string;

    /**
     * @param Ident $ident
     * @return string
     */
    public function logout(Ident $ident): ?string;

    /**
     * @param Refund $refund
     * @return string
     */
    public function refund(Refund $refund): ?string;

    /**
     * @param string $sessionId
     * @return mixed
     */
    public function setSessionId(string $sessionId);

    /**
     * @param DateTime $requestTime
     * @return mixed
     */
    public function setRequestTime(DateTime $requestTime);
}