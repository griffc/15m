<?php

namespace Tests\GameApiExample\Api;

use GameApiExample\Api\ApiFactory;
use GameApiExample\Api\Playson\PlaysonApiTypeOne;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use GuzzleHttp\Client;

class ApiFactoryTest extends TestCase
{

    public function testFactory_withInvalidType()
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('invalid api type');

        $mockClient = $this->createMock(Client::class);

        $gameApi = ApiFactory::create($mockClient, 'invalid type');
        
    }

    public function testFactory_withPlaysonTypeOne()
    {
        $mockClient = $this->createMock(Client::class);

        $gameApi = ApiFactory::create($mockClient, ApiFactory::PLAYSON_TYPE_ONE);

        $this->assertInstanceOf(PlaysonApiTypeOne::class, $gameApi);

    }

    public function testFactory_withPlaysonTypeTwo()
    {
        $mockClient = $this->createMock(Client::class);

        $gameApi = ApiFactory::create($mockClient, ApiFactory::PLAYSON_TYPE_ONE);

        $this->assertInstanceOf(PlaysonApiTypeOne::class, $gameApi);

    }

}