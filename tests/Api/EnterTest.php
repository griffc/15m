<?php

namespace Tests\GameApiExample\Api;

use GameApiExample\Api\ApiFactory;
use GameApiExample\Api\ApiInterface;
use GameApiExample\Api\Models\Enter;
use GameApiExample\Api\Models\Game;
use GameApiExample\Api\Models\Ident;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use DateTime;
use DateTimeZone;

class EnterTest extends TestCase
{

    public function testEnter()
    {

        $now = new DateTime('now', new DateTimeZone('UTC'));

        // mock the guzzle client
        $mockClient = $this->createMock(Client::class);

        // setup expectations

        $expectedXml = "<?xml version=\"1.0\"?>\n<server session=\"session id here\" time=\"{$now->format('c')}\"><enter id=\"id\" guid=\"guid\" key=\"a key\"><game name=\"game name\"/></enter></server>\n";

        $mockClient->expects($this->once())
            ->method('request')
            ->with('POST', '/enter', ['body' => $expectedXml]);


        // create params required for call
        $ident = new Ident('id', 'guid');
        $game = new Game('game name');
        $enter = new Enter($ident, 'a key', $game);


        /** @var ApiInterface $gameApi */
        $gameApi = ApiFactory::create($mockClient, ApiFactory::PLAYSON_TYPE_ONE);
        $gameApi->setSessionId('session id here');
        $gameApi->setRequestTime($now);



        $result = $gameApi->enter($enter);


    }

}